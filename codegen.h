#include <stack>
#include <typeinfo>
#include <llvm/IR/Module.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/Type.h>
#include <llvm/IR/DerivedTypes.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/PassManager.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/CallingConv.h>
#include <llvm/IR/IRPrintingPasses.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/Bitcode/ReaderWriter.h>
#include <llvm/Support/TargetSelect.h>
#include <llvm/ExecutionEngine/ExecutionEngine.h>
#include <llvm/ExecutionEngine/MCJIT.h>
#include <llvm/ExecutionEngine/GenericValue.h>
#include <llvm/Support/raw_ostream.h>

using namespace llvm;

class NProgram;

class CodeGenBlock {
   public:
    BasicBlock *block;
    Value *returnValue;
    std::map<std::string, Value *> locals;
};

class CodeGenContext {
    std::stack<CodeGenBlock *> blocks;
    Function *mainFunction;
    /* TODO: add currentFunction to context */

   public:
    Module *module;
    IRBuilder<> *builder;

    CodeGenContext() { module = new Module("main", getGlobalContext()); }

    void generateCode(NProgram &root);
    GenericValue runCode();
    std::map<std::string, Value *> &locals() { return blocks.top()->locals; }
    BasicBlock *currentBlock() { return blocks.top()->block; }
    void pushBlock(BasicBlock *block) {
        blocks.push(new CodeGenBlock());
        blocks.top()->returnValue = NULL;
        blocks.top()->block = block;

        if (blocks.size() == 1)
            builder = new IRBuilder<>(block);
        else
            builder->SetInsertPoint(block);
    }
    void popBlock() {
        CodeGenBlock *top = blocks.top();
        blocks.pop();
        delete top;
        
        if (blocks.size() == 0)
            delete builder;
        else
            builder->SetInsertPoint(blocks.top()->block);
    }
    void setCurrentReturnValue(Value *value) {
        blocks.top()->returnValue = value;
    }
    Value *getCurrentReturnValue() { return blocks.top()->returnValue; }
};
