#include <iostream>
#include <vector>
#include <llvm/IR/Value.h>

using namespace std;

class CodeGenContext;

class NFuncParam;
class NProgram;
class NTSDcl;
class NOp;
class NUnaryOp;
class NBinaryOp;
class NArithmetic;
class NConditional;
class NProgramPart;
class NFuncDcl;
class NStart;
class NFinish;
class NExpr;
class NConstVal;
class NBinaryExpr;
class NUnaryExpr;
class NBlockPart;
class NVarDcl;
class NStatement;
class NFuncCall;
class NAssignment;
class NSizeof;
class NBreak;
class NContinue;
class NReturnStmt;
class NLoopStmt;
class NAssignInit;
class NAssignChange;
class NBlock;
class NConstInt;
class NConstReal;
class NConstChar;
class NConstBool;
class NConstString;
class NConstLong;
class NExprStatement;
class NReturnStatement;
class NVariableDeclaration;
class NFunctionDeclaration;
class NCondStmt;
class NIfStmt;
class NCaseStmt;
class NForEach;
class NForRange;
class NSizeofParam;
class NType;
class NId;

class Node {
   public:
    virtual ~Node() {}
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
};

class NProgram : public Node {
   public:
    vector<NProgramPart*> parts;
    NProgram() {}
    virtual llvm::Value* codeGen(CodeGenContext& context);
};

class NTSDcl : public Node {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
};

class NOp : public Node {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
};

class NUnaryOp : public NOp {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
};

class NBinaryOp : public NOp {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
};

class NArithmetic : public NBinaryOp {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
};

class NConditional : public NBinaryOp {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
};

class NProgramPart : public Node {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
};

class NFuncDcl : public NProgramPart {
   public:
    /* this is temporary */
    NBlock& block;
    NFuncDcl(NBlock& block) : block(block) {}
    virtual llvm::Value* codeGen(CodeGenContext& context);
};

class NStart : public NProgramPart {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
};

class NFinish : public NProgramPart {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
};

class NExpr : public Node {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
};

class NSizeofParam : public Node {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
};

class NType : public NSizeofParam {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
};

class NFuncParam : public Node {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
};

class NId : public NFuncParam, public NSizeofParam, public NExpr {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
    // public:
    //  std::string name;
    //  NId(const std::string& name) : name(name) {}
    //  virtual llvm::Value* codeGen(CodeGenContext& context);
};

class NConstVal : public NFuncParam, public NExpr {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
};

class NBinaryExpr : public NExpr {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
    // public:
    //  NBinaryOp& op;
    //  NExpr& lhs, &rhs;
    //  NBinaryExpr(NExpr& lhs, NBinaryOp& op, NExpr& rhs)
    //      : lhs(lhs), rhs(rhs), op(op) {}
    //  virtual llvm::Value* codeGen(CodeGenContext& context);
};

class NUnaryExpr : public NExpr {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
};

class NBlockPart : public Node {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
};

class NVarDcl : public NBlockPart, public NProgramPart {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
};

class NStatement : public NBlockPart {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
};

class NFuncCall : public NExpr, public NStatement {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
    // public:
    //  NId& id;
    //  vector<NFuncParam*> params;
    //  NFuncCall(NId& id) : id(id) {}
    //  virtual llvm::Value* codeGen(CodeGenContext& context);
};

class NAssignment : public NStatement {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
};

class NSizeof : public NStatement {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
};

class NBreak : public NStatement {
   public:
    NBreak() {}
    virtual llvm::Value* codeGen(CodeGenContext& context);
};

class NContinue : public NStatement {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
};

class NReturnStmt : public NStatement {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
};

class NLoopStmt : public NStatement {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
};

class NAssignInit : public NAssignment {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
};

class NAssignChange : public NAssignment {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
    // public:
    //  NId& id;
    //  vector<NExpr*> indexes, values;
    //  NAssignChange(NId& id, const vector<NExpr*>& indexes,
    //                const vector<NExpr*>& values)
    //      : id(id), indexes(indexes), values(values) {}
    //  virtual llvm::Value* codeGen(CodeGenContext& context);
};

class NBlock : public NExpr {
   public:
    vector<NBlockPart*> parts;
    NBlock() {}
    virtual llvm::Value* codeGen(CodeGenContext& context);
};

class NConstInt : public NConstVal {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
    // public:
    //  long long value;
    //  NConstInt(long long value) : value(value) {}
    //  virtual llvm::Value* codeGen(CodeGenContext& context);
};

class NConstReal : public NConstVal {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
    // public:
    //  double value;
    //  NConstReal(double value) : value(value) {}
    //  virtual llvm::Value* codeGen(CodeGenContext& context);
};

class NConstChar : public NConstVal {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
};

class NConstBool : public NConstVal {
   public:
    bool value;
    NConstBool(bool value) : value(value) {}
    virtual llvm::Value* codeGen(CodeGenContext& context);
};

class NConstString : public NConstVal {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
};

class NConstLong : public NConstVal {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
};

class NCondStmt : public NStatement {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
};

class NIfStmt : public NCondStmt {
   public:
    vector<NExpr*> conditions;
    vector<NBlock*> blocks;
    NIfStmt() {}
    virtual llvm::Value* codeGen(CodeGenContext& context);
};

class NCaseStmt : public NCondStmt {
   public:
    NExpr *expr;
    vector<NConstVal*> constVals;
    vector<NBlock*> blocks;
    NCaseStmt() {}
    virtual llvm::Value* codeGen(CodeGenContext& context);
};

class NForEach : public NLoopStmt {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
};

class NForRange : public NLoopStmt {
   public:
    virtual llvm::Value* codeGen(CodeGenContext& context) = 0;
};
