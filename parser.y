%{
	#include "node.h"
    #include <cstdio>
    #include <cstdlib>
	NProgram *program; /* the top level root node of our final AST */

	extern int yylex();
	void yyerror(const char *s) { std::printf("Error: %s\n", s); std::exit(1); }
%}

/* Represents the many different ways we can access our data */
%union {
    Node *node;
    NProgram *program;
    NProgramPart *program_part;
    NFuncDcl *func_dcl;
    NBlock *block;
    NBlockPart *block_part;
    NStatement *statement;
    NBreak *break_stmt;
    NCondStmt *cond_stmt;
    NCaseStmt *case_stmt;
    NIfStmt *if_stmt;
    NExpr *expr;
    NConstVal *const_val;
    NConstBool *const_bool;
    std::string *string;
    int token;
}

/* Define our terminal symbols (tokens). This should
   match our tokens.l lex file. We also define the node type
   they represent.
 */
%token <string> TIDENTIFIER TINTEGER TDOUBLE
%token <token> TCEQ TCNE TCLT TCLE TCGT TCGE TEQUAL
%token <token> TLPAREN TRPAREN TLBRACE TRBRACE TCOLON TCOMMA TDOT
%token <token> TPLUS TMINUS TMUL TDIV

%token <token> TBOOL TBREAK TCASE TCHAR TCONTINUE TELSE TFALSE TIF TINT TLONG
%token <token> TIN TRANGE TREAL TRETURN TSTRING TTRUE TWHILE TELIF TTO TREAD
%token <token> TWRITE TSTRLEN TCONCAT TDEFAULT TINDENT TDEDENT


/* Define the type of node our nonterminal symbols represent.
   The types refer to the %union declaration above. Ex: when
   we call an ident (defined by union type ident) we are really
   calling an (NIdentifier*). It makes the compiler happy.
 */
%type <program> program
%type <program_part> program_part
%type <func_dcl> func_dcl
%type <block> block iblock
%type <block_part> block_part
%type <statement> statement
%type <break_stmt> break_stmt
%type <cond_stmt> cond_stmt
%type <case_stmt> case_stmt case_h case_b
%type <if_stmt> if_stmt if_h
%type <expr> expr
%type <const_val> const_val
%type <const_bool> const_bool
// %type <token>

/* Operator precedence for mathematical operators */
%left TPLUS TMINUS
%left TMUL TDIV

%start program

%%

program : program program_part { $1->parts.push_back($2); }
        | /* blank */ { program = new NProgram(); }
        ;

program_part : func_dcl { $$ = $1; }
             ;

func_dcl : block { $$ = new NFuncDcl(*$1); }
         ;

iblock : TINDENT block TDEDENT { $$ = $2; }
       ;

block : block block_part { $1->parts.push_back($2); }
      | block_part { $$ = new NBlock(); $$->parts.push_back($1); }
      ;

block_part : statement { $$ = $1; }
           ;

statement : cond_stmt { $$ = $1; }
          | break_stmt { $$ = $1; }
          ;

break_stmt : TBREAK { $$ = new NBreak(); }

cond_stmt : if_stmt { $$ = $1; }
          | case_stmt { $$ = $1; }
          ;

/* case */
case_stmt : TCASE expr TCOLON TINDENT case_b TDEDENT { $5->expr = $2; }
          ;

case_b : case_h
       | case_h TDEFAULT TCOLON iblock { $1->blocks.push_back($4); }
       ;

case_h : /* blank */ { $$ = new NCaseStmt(); }
       | case_h const_val TCOLON iblock { $1->constVals.push_back($2); $1->blocks.push_back($4); }
       ;
/* end of case */

/* if */
if_stmt : if_h TELSE TCOLON iblock { $1->blocks.push_back($4); }
        | if_h
        ;

if_h : TIF expr TCOLON iblock { $$ = new NIfStmt(); $$->conditions.push_back($2); $$->blocks.push_back($4); }
     | if_h TELIF expr TCOLON iblock { $1->conditions.push_back($3); $1->blocks.push_back($5); }
     ;
/* end of if */

expr : const_val { $$ = $1; }
     ;

const_val : const_bool { $$ = $1; }
          ;

const_bool : TTRUE { $$ = new NConstBool(true); }
           | TFALSE { $$ = new NConstBool(false); }
           ;

%%
