%{
#include <string>
#include "node.h"
#include "parser.hpp"

#define SAVE_TOKEN  yylval.string = new std::string(yytext, yyleng)
#define TOKEN(t)    (yylval.token = t)

/* globals to track current indentation */
int current_line_indent = 0;   /* indentation of the current line */
int indent_level = 0;          /* indentation level passed to the parser */
%}

/* start state for parsing the indentation */
%x indent
/* normal start state for everything else */
%s normal

%option noyywrap

%%

<indent>" "                     { current_line_indent += 1; }
<indent>"\t"                    { current_line_indent += 4; }
<indent>"\n"                    { current_line_indent = 0; /*ignoring blank line */ }
<indent>.                       {
                                    unput(*yytext);
                                    current_line_indent /= 4;
                                    if (current_line_indent > indent_level) {
                                        indent_level += 1;
                                        return TINDENT;
                                    } else if (current_line_indent < indent_level) {
                                        indent_level -= 1;
                                        return TDEDENT;
                                    } else {
                                        BEGIN normal;
                                    }
                                }

<normal>"\n"                    { current_line_indent = 0; BEGIN indent; }

[ \t\f\r\v]					    ;

"bool"                          return TOKEN(TBOOL);
"break"                         return TOKEN(TBREAK);
"case"                          return TOKEN(TCASE);
"char"                          return TOKEN(TCHAR);
"continue"                      return TOKEN(TCONTINUE);
"else"                          return TOKEN(TELSE);
"false"                         return TOKEN(TFALSE);
"if"                            return TOKEN(TIF);
"int"                           return TOKEN(TINT);
"long"                          return TOKEN(TLONG);
"in"                            return TOKEN(TIN);
"range"                         return TOKEN(TRANGE);
"real"                          return TOKEN(TREAL);
"return"                        return TOKEN(TRETURN);
"string"                        return TOKEN(TSTRING);
"true"                          return TOKEN(TTRUE);
"while"                         return TOKEN(TWHILE);
"elif"                          return TOKEN(TELIF);
"to"                            return TOKEN(TTO);
"read"                          return TOKEN(TREAD);
"write"                         return TOKEN(TWRITE);
"strlen"                        return TOKEN(TSTRLEN);
"concat"                        return TOKEN(TCONCAT);
"default"                       return TOKEN(TDEFAULT);

[a-zA-Z_][a-zA-Z0-9_]*          SAVE_TOKEN; return TIDENTIFIER;
[0-9]+\.[0-9]*                  SAVE_TOKEN; return TDOUBLE;
[0-9]+					        SAVE_TOKEN; return TINTEGER;

"="						        return TOKEN(TEQUAL);
"=="				          	return TOKEN(TCEQ);
"!="			          		return TOKEN(TCNE);
"<"				          		return TOKEN(TCLT);
"<="	          				return TOKEN(TCLE);
">"				          		return TOKEN(TCGT);
">="					        return TOKEN(TCGE);

"("	          					return TOKEN(TLPAREN);
")"					          	return TOKEN(TRPAREN);
"{"         					return TOKEN(TLBRACE);
"}"					          	return TOKEN(TRBRACE);
":"                             return TOKEN(TCOLON);

"."         					return TOKEN(TDOT);
","				          		return TOKEN(TCOMMA);

"+"				          		return TOKEN(TPLUS);
"-"		          				return TOKEN(TMINUS);
"*"		          				return TOKEN(TMUL);
"/"				          		return TOKEN(TDIV);

.                               printf("Unknown token!\n"); yyterminate();

%%
